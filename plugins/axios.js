import Vue from 'vue'
import axios from 'axios'
import ls from 'local-storage'

// let auth = ls('auth') || {}
// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || ''
// if (auth.access_token) {
//   axios.defaults.headers.common['Authorization'] = `Bearer ${auth.access_token}`
// }
axios.defaults.headers.post['Content-Type'] = 'application/json';

let config = {
    baseURL: process.env.VUE_APP_API_URL || ''
    // timeout: 60 * 1000, // Timeout
    // withCredentials: true // Check cross-site Access-Control
};

const _axios = axios.create(config);

_axios.interceptors.request.use(config => {
    config.headers.Authorization = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU4Y2VmZjg1MTRjMDg3NTAzZTU1ODk2NWMzYjIyMGRjZDFjMWFiMjJlZTczYTRmNDczMWZhYTE1NGM4ZWVkZWU2OTRkMjlhNmI1OWQ4MjkxIn0.eyJhdWQiOiI5IiwianRpIjoiNThjZWZmODUxNGMwODc1MDNlNTU4OTY1YzNiMjIwZGNkMWMxYWIyMmVlNzNhNGY0NzMxZmFhMTU0YzhlZWRlZTY5NGQyOWE2YjU5ZDgyOTEiLCJpYXQiOjE1MzgxMjM4NzQsIm5iZiI6MTUzODEyMzg3NCwiZXhwIjoxNTY5NjU5ODc0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.jZmCMU_M9aaFmRjMSVwTgqTLRwdp351pv0LXLbV99xPeF6hUDvQz9rREXot2t4wFaa1eWoCkS0UzPEsBhmFkm0AuQGGtlsgD8vV42MeyzQbA1V2LSdw3fpUTPejeifkW1trnOPoLtp-_SrSjsrVlYyBPEIQ3hNoEAMKr4v8mda4H8O08_nGPAIA99hLPTwAuuaB4V8oCQVwrSWcPl2y8GYjXfltI5c0c7pl9BuBu7TXn2n7yBRPem_lQK02zozBSssJecdui6PchZD3sMR-tcvCAjFv9TLdgNCy5zkzwdxyOIBfs3y3GX6dvH6Yddx4DtqpxLd9mqVW-nlQ8aa03iasGIKBc1OU35tQh04fGSdSeNMlxAUEaL5Upa0jSjh9vlf6xWNCh7EFkhq013gQAM0GsJdXGcWdG0hrFK7I7GqaU1271Sbu2woRpcjO2XozppRfRXe_JeB_7AAYXDZXKfOirpESwFzmbjSxPVoxpLxo0iOLkytKF5IIFMVZX6gQsxjEsVIFWKSPoR4HfnnDMI--z01hEu-GO-Kf8_AeyjeVaJdwQIrS6q4bfGkFN6yk3Qi2K3INHiRVVwog_9fyoL5RY5TlAVTpaQ5n-rO6UUFxTf9k_PEAV_JeGPap6PkgTVDsFAqvx3BSjZLGdjY2rUW7Y_m7OINtRM-BcRie_vic';
    return config
}, error => {
    // Do something with request error
    return Promise.reject(error)
});


// Add a response interceptor
_axios.interceptors.response.use(
    function (response) {
        // Do something with response data
        return response
    },
    function (error) {
        // Do something with response error
        return Promise.reject(error)
    }
);

Plugin.install = function (Vue, options) {
    Vue.axios = _axios;
    window.axios = _axios;
    Object.defineProperties(Vue.prototype, {
        axios: {
            get () {
                return _axios
            }
        },
        $axios: {
            get () {
                return _axios
            }
        }
    })
};

Vue.use(Plugin);

export default _axios
