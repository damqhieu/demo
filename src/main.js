// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store';
import 'vuetify/dist/vuetify.min.css'
import './assets/custom.css'
import '@/api/config.js'
// import './plugins/axios'


//toastr js

import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
window.toastr = require('toastr');
Vue.use(VueToastr2);

// validate
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

Vue.config.productionTip = false;

import Vuetify from 'vuetify'

Vue.use(Vuetify,{
    iconfont: 'fa',
});


new Vue({
    el: '#app',
    router,
    store,
    components: {App},
    template: '<App/>'
});
