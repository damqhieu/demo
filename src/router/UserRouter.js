import User from '../View/User/Index.vue';
import NewUser from '../View/User/Create.vue';
import EditUser from '../View/User/Edit.vue';

export default [
    {
        path: '/users',
        name: 'user',
        component: User
    },
    {
        path: '/users/create',
        name: 'user.create',
        component: NewUser
    },
    {
        path: '/user/edit/:id',
        name: 'user.edit',
        component: EditUser
    }
];
