import Vue from 'vue';
import Router from 'vue-router';

import UserRouter from './UserRouter.js';
import CategoryRouter from './CategoryRouter.js';

Vue.use(Router);

const UserPosts = { template: '<div>Home</div>' };

export default new Router({
    mode: 'history',
    base: '/admin',
    routes: [
        {
            path: '/',
            redirect: 'home',
        },
        {
            path: '/home',
            name: 'home',
            component: UserPosts,
        },
        ...UserRouter,
        ...CategoryRouter,
    ],
});
