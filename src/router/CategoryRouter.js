import Category from '../View/Category/Index.vue';
import NewCategory from '../View/Category/Create.vue';
import EditCategory from '../View/Category/Edit.vue';

export default [
    {
        path: '/categories',
        name: 'category',
        component: Category
    },
    {
        path: '/categories/create',
        name: 'category.create',
        component: NewCategory
    },
    {
        path: '/categories/edit/:id',
        name: 'category.edit',
        component: EditCategory
    }
];
