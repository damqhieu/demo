import {
    NOTIFY_INSTANCE,
    NOTIFY_TIME,
    NOTIFY_TEXT,
    NOTIFY_COLOR
} from '../mutation-types'

/**
 * state
 */
const state = {
    snackbar: false,
    snackbarTimeout: 6000,
    snackbarText: '',
    snackbarColor: ''
};

/**
 * actions
 */
const actions = {
    showNotify ({ commit }, payload = {}) {
        let { timeout, text, color } = payload;
        timeout && commit(NOTIFY_TIME, timeout);
        text && commit(NOTIFY_TEXT, text);
        color && commit(NOTIFY_COLOR, color);
        commit(NOTIFY_INSTANCE, true)
    },
    setNotifyInstance ({ commit }, payload) {
        commit(NOTIFY_INSTANCE, payload)
    }
};

/**
 * mutations
 */
const mutations = {
    [NOTIFY_INSTANCE]: (state, instance) => {
        state.snackbar = instance
    },
    [NOTIFY_TIME]: (state, timeout) => {
        state.snackbarTimeout = timeout
    },
    [NOTIFY_TEXT]: (state, text) => {
        state.snackbarText = text
    },
    [NOTIFY_COLOR]: (state, color) => {
        state.snackbarColor = color
    }
};

/**
 * getters
 */
const getters = {
    snackbar: (state) => state.snackbar,
    snackbarTimeout: (state) => state.snackbarTimeout,
    snackbarText: (state) => state.snackbarText,
    snackbarColor: (state) => state.snackbarColor
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
