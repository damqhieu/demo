import axios from 'axios'
import router from '@/router'
import {
    FETCHING_RESOURCES,
    FETCHING_RESOURCES_DONE,
    FETCHING_RESOURCES_FAIL
} from '../mutation-types'
import {
    forIn
} from 'lodash'

/**
 * state
 */
const state = {
    fetchApi: false,
    apiErrors: null
};

/**
 * actions
 */
const actions = {
    async fetchApi({commit, dispatch}, payload = {}) {
        let {
            success,
            error,
            showWarnings
        } = payload;
        showWarnings = showWarnings === undefined ? true : showWarnings;

        try {
            commit(FETCHING_RESOURCES, true);
            let response = await axios.request(payload);
            commit(FETCHING_RESOURCES_DONE);
            success && success(response.data)
        } catch (e) {
            let err = null;
            if (e.response) {
                if (showWarnings) {
                    switch (e.response.status) {
                        case 422:
                            let msg = [];
                            forIn(e.response.data.errors, (err, idx) => {
                                msg.push('&bullet; ' + err[0])
                            });
                            console.log(msg)
                            toastr.error( msg.join('<br />'));
                            dispatch('notify/showNotify', {
                                text: msg.join('<br />'),
                                color: 'warning'
                            },{ root: true});
                            break;
                        case 403:
                            dispatch('notify/showNotify', {
                                text: 'Bạn không có quyền thực hiện tác vụ này',
                                color: 'warning'
                            });
                            router.push({
                                name: 'forbidden'
                            });
                            break;
                        case 401:
                            dispatch('notify/showNotify', {
                                text: 'Phiên đăng nhập hết hạn vui lòng đăng nhập',
                                color: 'warning'
                            });
                            dispatch('notify/clearLogged');
                            router.push({
                                name: 'login'
                            });
                            break;
                        case 500:
                            dispatch('notify/showNotify', {
                                text: 'Máy chủ sảy ra sự cố vui lòng thử lại sau',
                                color: 'error'
                            });
                            router.push({
                                name: 'opps'
                            });
                            break
                    }
                }
                err = e.response
            } else if (e.request) {
                err = e.request
            } else {
                err = e.message
            }
            commit(FETCHING_RESOURCES_FAIL, err);
            error && error(err)
        } finally {
            commit(FETCHING_RESOURCES, false)
        }
    }
};

/**
 * mutations
 */
const mutations = {
    [FETCHING_RESOURCES_FAIL]: (state, err) => {
        state.apiErrors = err.response
    },
    [FETCHING_RESOURCES_DONE]: (state) => {
        state.apiErrors = null
    },
    [FETCHING_RESOURCES]: (state, isFechApi) => {
        state.fetchApi = isFechApi
    }
};

/**
 * getters
 */
const getters = {
    allApiErrors: (state) => state.apiErrors,
    isFetchingApi: (state) => state.fetchApi
};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
