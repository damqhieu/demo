import {
    SET_CATEGORY,
    SET_DETAIL_CATEGORY,
    REMOVE_CATEGORY,
} from '../mutation-types'

import axios from 'axios'


import {
    forIn
} from 'lodash'

import 'vue-toastr-2/dist/vue-toastr-2.min.css'

/**
 * Category state
 */
const state = {
    categories: [],
    category: {},
};

/**
 * Category actions
 */
//


const actions = {
    getAllCategory ({commit, dispatch }) {
        dispatch(
            'api/fetchApi',
            {
                url: 'categories',
                method: 'GET',
                success: (response) => {
                    commit(SET_CATEGORY, response.data);
                }
            },
            { root: true}
        )
    },
    createCategory ({commit, dispatch}, payload) {
        let { category, cb } = payload
        dispatch('api/fetchApi', {
            url: 'categories',
            method: 'POST',
            data: category,
            success: cb
        }, {root: true})
    },
    removeCategory ({commit, dispatch}, payload) {
        let { id, cb } = payload
        dispatch('api/fetchApi', {
            url: `categories/${id}`,
            method: 'DELETE',
            success: cb
        }, {root: true})
    },
    getCategory ({commit, dispatch}, payload) {
        let { id, cb } = payload
        dispatch('api/fetchApi', {
            url: `categories/${id}`,
            method: 'GET',
            success: (response) => {
                commit(SET_DETAIL_CATEGORY, response.data);
            }
        }, {root: true})
    },
    updateCategory ({commit, dispatch}, payload) {
        let { id, category, cb } = payload
        dispatch('api/fetchApi', {
            url: `categories/${id}`,
            method: 'PUT',
            data : category,
            success: cb
        }, {root: true})
    },
};

/**
 * Category mutations
 */
const mutations = {

    /**
     * Set Category data to state
     */
    [SET_CATEGORY](state, categories) {
        state.categories = categories
    },
    [SET_DETAIL_CATEGORY] (state, category) {
        state.category = category
    },

};

/**
 * Category getters
 */
const getters = {

    allCategories: (state) => state.categories,
    categoryDetail: (state) => state.category,

};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
}
