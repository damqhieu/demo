
// Điều này cho phép mã tận dụng lợi thế của công cụ như linters và
// đặt tất cả các hằng số trong một tệp cho phép cộng tác viên của bạn xem nhanh
// những đột biến nào có thể xảy ra trong toàn bộ ứng dụng


export const TABLE_MUTATION = 'TABLE_MUTATION';
export const SET_CATEGORY = 'SET_CATEGORY';
export const SET_DETAIL_CATEGORY = 'SET_DETAIL_CATEGORY';
export const REMOVE_CATEGORY = 'REMOVE_CATEGORY';

// api
export const FETCHING_RESOURCES_DONE = 'API/FETCHING_RESOURCES_DONE';
export const FETCHING_RESOURCES_FAIL = 'API/FETCHING_RESOURCES_FAIL';
export const FETCHING_RESOURCES = 'API/FETCHING_RESOURCES';

// notify
export const NOTIFY_INSTANCE = 'NOTIFY/INSTANCE';
export const NOTIFY_TIME = 'NOTIFY/TIME';
export const NOTIFY_TEXT = 'NOTIFY/TEXT';
export const NOTIFY_COLOR = 'NOTIFY/COLOR';
