import Vue from 'vue'
import Vuex from 'vuex'
import category from './modules/category.js';
import api from './modules/api.js';
import notify from './modules/notify.js';
Vue.use(Vuex);

const state = {};
const mutations = {};
const actions = {};
const getters = {};


export default new Vuex.Store({
    modules: {
        api,
        category,
        notify
    },
    state,
    mutations,
    actions,
    getters
});

